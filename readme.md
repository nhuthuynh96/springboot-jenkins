# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/maven-plugin/)

#document: 
https://stackoverflow.com/questions/27767264/how-to-dockerize-maven-project-and-how-many-ways-to-accomplish-it
#build image
docker build -t shahid-demo:01 .
#run image
docker run -d -p 8080:8080 --rm -it shahid-demo:01

#Docker jenkins
## build docker jenkins
### build with existing image
docker image build -t jenkins-docker .
### build with docker file
docker image build -f JenkinDockerfile -t jenkins-docker . 
## Run jenkins container
docker run --name jenkins-docker -d -p 8080:8080 jenkins-docker:latest
## Setup jenkins build with docker
https://www.edureka.co/community/55640/jenkins-docker-docker-image-jenkins-pipeline-docker-registry